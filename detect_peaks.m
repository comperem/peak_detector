function [peak_t,peak_y,risingFlagLog] = detect_peaks(t,sig,threshCntRising,threshCntFalling)
%
% A peak detection algorithm that counts consecutive rising or falling
% counts. A risingFlag captures the current rising state after either
% the rising or falling counter is met or exceeded.
%
% Rising and falling counters are reset after each sample discovered
% to the contrary.
%
% Rising or falling counter thresholds are asymmetric and both are
% required to trigger a detected peak.
%
% Marc Compere, comperem@erau.edu
% created : 08 Dec 2019
% modified: 26 Dec 2021
%
%
% --------------------------------------------------------------
% Copyright 2021 - 2022 Marc Compere
%
% This file is part of peak_detector which
% is open source software licensed under the GNU GPLv3.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3
% as published by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License version 3 for more details.
%
% A copy of the GNU General Public License is included with MoVE
% in the COPYING file, or see <https://www.gnu.org/licenses/>.
% --------------------------------------------------------------

if nargin<4
    threshCntFalling=4; % threshold for consecutive falling values to determing 'falling' state (risingFlag==0) 
end
if nargin<3
    threshCntRising=3; % threshold for assigning rising state (risingFlag==1)
end


n=length(t);
peak_t=[]; % peak timestamp array
peak_y=[]; % peak value
y_last = sig(1);
risingFlag = sig(2) > sig(1); % (T/F) currently rising or falling? (positive slope or negative slope?)
risingFlagLog=risingFlag;
fallingCnt = 0;
risingCnt  = 0;

for i=2:n %   i=i+1
    
    ithPtRising = (sig(i)>sig(i-1)); % (0/1) is this particular point rising or falling?
    
    if (ithPtRising==1)
        risingCnt = risingCnt + 1;
        fallingCnt=0; % reset falling counter
    else
        fallingCnt = fallingCnt + 1;
        risingCnt=0; % reset rising counter
    end
    
    if (risingCnt>=threshCntRising) % require this many consecutive rising points to confirm rising
        risingFlag=1; % assign risingFlag after signal is rises this many times without resetting to falling 
    end
    
    if (fallingCnt>=threshCntFalling) % require this many consecutive rising points to confirm rising
        if (risingFlag==1) % if falling, but were recently rising, we just passed a peak 
            % get peak a few indices ago
            howFarBack = fallingCnt;
            peak_t = [ peak_t , t(i-howFarBack)   ];
            peak_y = [ peak_y , sig(i-howFarBack) ];
        end
        
        risingFlag=0; % assign risingFlag after signal is rises this many times without resetting to falling 
    end
    
    risingFlagLog(i) = risingFlag; % log for output record
    
end

