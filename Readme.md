# Simple Peak Detector

This is a complete working example of two custom peak detection algorithms,
called `detect_peaks` and `detect_peaks_with_rise` developed specifically to
detect peaks in time-series experimental data.

This code was tested and developed using plain vanilla Matlab R2020a
with no special toolboxes. It should run on about any version of Matlab before
or after this version.

![peaks detected on 4 data sets](detect_peaks_logo_gitlab.png "Detected Peaks!")

## Install and Run:
Download all files and run in just about any version of Matlab:

	peak_detector_cases_with_filters.m

The result will be generated in 1 Matlab figure and, if the `makeFigureImages`
flag is set, the figures will be saved to the ./outputs folder. You may need
to create the `./outputs` folder.

## License:
All code is released as free, open-source software under the GPLv3 license.

## Algorithm descriptions:

Both algorithms are described below: <br>

The simple `detect_peaks` algorithm:
- Uses separate rising and falling counters to assign a
`risingState` with user-provided thresholds.
- Rising count is independent of falling count.
- A peak is detected when enough rising counts are detected first,
    then enough falling counts have successfully been detected, in sequence.

The `detect_peaks_with_rise` algorithm is `detect_peaks` but including a certain percentage rise from the previous peak.
`detect_peaks_with_rise`:
- adds an additional constraint before a peak is detected:
- The total rise from the previous peak must be greater than a certain percentage of the total data range. This reduces chances of multiple unwanted repeated peaks.
- Drops in the data since the last detected peak are integrated.

A simple Infinite Impulse Repsonse (IIR) filter is included and may be helpful
with especially noisy data, but is not always necessary.

## Example data sets:
Four experimental datasets are provided that illlustrate both algorithms:
  - sinuisoidal, or periodic
  - constant but noisy data with 3 successive peaks
  - rising, stair-step sequence A
  - rising, stair-step sequence B


***

Marc Compere, phd <comperem@erau.edu> <br>
https://sites.google.com/site/comperem <br>
created : 28 Dec 2021 <br>
modified: 11 Jan 2021
