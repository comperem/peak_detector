function [signals,avgFilter,stdFilter] = thresholdingAlgo_with_Z_scores(y,lag,threshold,influence)
%
% Peak detector, outlier detection using Z-scores
% Thresholding algorithm using Z-scores: https://stackoverflow.com/questions/22583391/peak-signal-detection-in-realtime-timeseries-data/54507329#54507329
%
% lag:  the lag parameter determines how much your data will be smoothed and how
%       adaptive the algorithm is to changes in the long-term average of the data.
%       (moving window size N)
%
% influence: this parameter determines the influence of signals on the algorithm's
%       detection threshold. If put at 0, signals have no influence on the threshold,
%       such that future signals are detected based on a threshold that is calculated
%       with a mean and standard deviation that is not influenced by past signals.
%
% threshold: the threshold parameter is the number of standard deviations from the
%       moving mean above which the algorithm will classify a new datapoint as being
%       a signal. For example, if a new datapoint is 4.0 standard deviations above the
%       moving mean and the threshold parameter is set as 3.5, the algorithm will
%       identify the datapoint as a signal.
%
%
% Copied from Stackoverflow.com
% mdc, 02 Aug 2020



% Initialise signal results
signals = zeros(length(y),1);

% Initialise filtered series
filteredY = y(1:lag+1);

% Initialise filters
avgFilter(lag+1,1) = mean(y(1:lag+1));
stdFilter(lag+1,1) = std(y(1:lag+1));

% Loop over all datapoints y(lag+2),...,y(t)
for i=lag+2:length(y)
    % If new value is a specified number of deviations away
    if abs(y(i)-avgFilter(i-1)) > threshold*stdFilter(i-1)
        if y(i) > avgFilter(i-1)
            % Positive signal
            signals(i) = 1;
        else
            % Negative signal
            signals(i) = -1;
        end
        % Make influence lower
        filteredY(i) = influence*y(i)+(1-influence)*filteredY(i-1);
    else
        % No signal
        signals(i) = 0;
        filteredY(i) = y(i);
    end
    % Adjust the filters
    avgFilter(i) = mean(filteredY(i-lag:i));
    stdFilter(i) = std(filteredY(i-lag:i));
end

% Done, now return results
end



