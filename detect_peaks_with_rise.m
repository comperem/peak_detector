function [peak_t,peak_y,risingFlagLog,rise_log] = detect_peaks_with_rise(t,sig,threshCntRising,threshCntFalling,riseThresh)
%
% A peak detection algorithm that counts consecutive rising or falling
% counts. A risingFlag captures the current rising state after either
% the rising or falling counter is met or exceeded.
%
% Rising and falling counters are reset after each sample discovered
% to the contrary.
%
% Rising or falling counter thresholds are asymmetric and both are
% required to trigger a detected peak.
%
% detect_peaks_with_rise.m also has a requirement for each peak to rise
% a certain percentage above the successive peak or any valley between.
% This prevents subsequent peaks within the same plateau.
%
% Marc Compere, comperem@erau.edu
% created : 08 Dec 2019
% modified: 27 Dec 2021
%
%
% --------------------------------------------------------------
% Copyright 2021 - 2022 Marc Compere
%
% This file is part of peak_detector which
% is open source software licensed under the GNU GPLv3.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3
% as published by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License version 3 for more details.
%
% A copy of the GNU General Public License is included with MoVE
% in the COPYING file, or see <https://www.gnu.org/licenses/>.
% --------------------------------------------------------------

if nargin<4
    threshCntFalling=4; % threshold for consecutive falling values to determing 'falling' state (risingFlag==0) 
end
if nargin<3
    threshCntRising=3; % threshold for assigning rising state (risingFlag==1)
end


n=length(t);
peak_t=[]; % peak timestamp array
peak_y=[]; % peak value
risingFlag = sig(2) > sig(1); % (T/F) currently rising or falling? (positive slope or negative slope?)
risingFlagLog=risingFlag;
fallingCnt = 0;
risingCnt  = 0;

risePct=0.0;
riseLast=sig(1);
rise_log=[];
range=max(sig)-min(sig); % (units of signal)

for i=2:n %   i=i+1
    
    ithPtRising = (sig(i)>sig(i-1)); % (0/1) is this particular point rising or falling?
    
    riseLast = min(riseLast,sig(i)); % look for dips in the trace between peaks 
    
    if (ithPtRising==1)
        risingCnt = risingCnt + 1;
        fallingCnt=0; % reset falling counter
        risePct=(sig(i)-riseLast)/range; % (%)
    else
        fallingCnt = fallingCnt + 1;
        risingCnt=0; % reset rising counter
        %risingAccum=0; % reset rising accumulator
        %fallingAccum=fallingAccum-sig(i); % falling accumulator
    end
    
    if (risingCnt>=threshCntRising) % require this many consecutive rising points to confirm rising
        risingFlag=1; % assign risingFlag after signal is rises this many times without resetting to falling 
    end
    
    if (fallingCnt>=threshCntFalling) % require this many consecutive rising points to confirm rising
        if (risingFlag==1) && (risePct>=riseThresh) % if falling, but were recently rising, we just passed a peak 
            str=sprintf('cnt= %i, riseDist=%0.2f(%%)',length(rise_log)+1,risePct); disp(str)
            
            % get peak a few indices ago
            howFarBack = fallingCnt;
            peak_t   = [ peak_t   , t(i-howFarBack)   ];
            peak_y   = [ peak_y   , sig(i-howFarBack) ];
            rise_log = [ rise_log , risePct          ]; % (%)
            
            riseLast=peak_y(end); % reset riseLast after each peak
        end
        
        risingFlag=0; % assign risingFlag after signal is rises this many times without resetting to falling 
    end
    
    risingFlagLog(i) = risingFlag; % log for output record
    
end

