function y = iir(t,u,tau)

% IIR - Infinite Impulse Response filter
%
% tau - first order filter time constant in seconds
%
% Marc Compere, comperem@gmail.com
% created : 04 Apr 2016
% modified: 08 Dec 2019
%
%
% --------------------------------------------------------------
% Copyright 2021 - 2022 Marc Compere
%
% This file is part of peak_detector which
% is open source software licensed under the GNU GPLv3.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3
% as published by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License version 3 for more details.
%
% A copy of the GNU General Public License is included with MoVE
% in the COPYING file, or see <https://www.gnu.org/licenses/>.
% --------------------------------------------------------------

% pre-allocate arrays
y=zeros(length(t),1);
%z=zeros(length(t),1);

y(1)=u(1);
%y(1)=0; % filter's IC
%z(1)=0; % filter's IC
%zLast=z(1);


% dt is discerned by sample time indicated by values in t array
dt = sum(diff(t)) / (length(t)-1);

% IIR variables
alpha = dt/(tau+dt); % see Centikunt p.77 for backward difference formula

for k=2:length(t),
    
	% IFI - Infinite Impulse Response (b/c it's got feedback)
	% two first order digital filters with sample time dt and time constant tau
	y(k) = alpha*u(k) + (1-alpha)*y(k-1);
    
    % z is an identical filter with slightly different programming method
	%z(k) = alpha*u(k) + (1-alpha)*zLast;
	%zLast = z(k);

end





