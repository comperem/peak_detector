function p = fir(t,u,windowLen)

% FIR - Finite Impulse Response filter
%
% Marc Compere, comperem@gmail.com
% created : 04 Apr 2016
% modified: 08 Dec 2019
%
%
% --------------------------------------------------------------
% Copyright 2021 - 2022 Marc Compere
%
% This file is part of peak_detector which
% is open source software licensed under the GNU GPLv3.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3
% as published by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License version 3 for more details.
%
% A copy of the GNU General Public License is included with MoVE
% in the COPYING file, or see <https://www.gnu.org/licenses/>.
% --------------------------------------------------------------

p(1)=u(1); % filter's IC
%p(1)=0; % filter's IC

u=u(:); % ensure u is a column vector

for k=2:length(t),

	% FIR - Finite Impulse Response (no feedback)
	if (k<windowLen),
		w = ones(k,1)/k; % filter weights on each of the previous samples
		windowLow = 1;
	else
		w = ones(windowLen,1)/windowLen; % filter weights on each of the previous samples
		windowLow = k-windowLen+1;
	end
	p(k) = sum( u(k:-1:windowLow) .* w );

end





