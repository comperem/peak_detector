% Matlab-based peak detection algorithms and simple filters.
%
% detect_peaks.m is a peak detection algorithm is based on a successive
% pattern of sequential rising, then sequential falling samples.
%
% because the risingCount and fallingCount are reset each time the opposing
% sample-sequence is detected, this approachs works best with smoothed
% signals that can still contain noise, but with smoothly varying ups and
% downs.
%
% detect_peaks_with_rise.m is an approach to leverage the algorithm in
% detect_peaks.m, then add a certain threshold of increase from the
% previous peak detected. This acts as an inhibitor to the somewhat
% easily triggered detect_peaks.m algorithm. The rise from one peak to the
% next is with respect to the lowest point discovered since the last peak.
%
% The thresholdingAlgo is unused but still included here because it's
% excellent. It is an approach based on Z-scores located here:
% https://stackoverflow.com/questions/22583391/peak-signal-detection-in-realtime-timeseries-data
%
% Infinite Impulse Response (IIR) and Finite Impulse Response (FIR) are
% also included. Only IIR is used in this script but FIR and Z-scores based
% algorhtm is included for experimentation.
%
% Marc Compere, comperem@erau.edu
% created : 08 Dec 2019
% modified: 28 Dec 2021
%
%
% --------------------------------------------------------------
% Copyright 2021 - 2022 Marc Compere
%
% This file is part of peak_detector which
% is open source software licensed under the GNU GPLv3.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3
% as published by the Free Software Foundation.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License version 3 for more details.
%
% A copy of the GNU General Public License is included with MoVE
% in the COPYING file, or see <https://www.gnu.org/licenses/>.
% --------------------------------------------------------------

clear ; clear all ; clear functions

makeFigureImages=1; % (0/1) save figures or just plot?


signalSelect = 4; % (1/2/3/4)
if signalSelect==1
    offset=40;
    A = 20;
    randMag=0.1*A;
    f=3;
    tf=3;%0.5;
    %tf=0.5;
    dt=0.001; % (s)
    t=0:dt:tf;
    n=length(t);
    sig = offset + A*sin(2*pi*f*t) + randMag*(rand(1,n)-0.5);
    %sig(14)=60; % workee
    %sig(14)=0; % no-workee
    
    threshCntRising=10; % threshold for detecting top of the peak
    threshCntFalling=2; % remember: you can only detect a peak when both thresholds are exceeded 
    riseThresh=0.00; % (%) percent of total range
elseif signalSelect==2
    sig = [1 1 1.1 1 0.9 1 1 1.1 1 0.9 1 1.1 1 1 0.9 1 1 1.1 1 1 1 1 1.1 0.9 1 1.1 1 1 0.9 1 1.1 1 1 1.1 1 0.8 0.9 1 1.2 0.9 1 1 1.1 1.2 1 1.5 1 3 2 5 3 2 1 1 1 0.9 1 1 3 2.6 4 3 3.2 2 1 1 0.8 4 4 2 2.5 1 1 1];
    
    % extend signal with starting average
    sigAvg=sum(sig(1:20))/20;
    nZerosToAppend=50;
    %sig=[sig,sigAvg*ones(1,nZerosToAppend)];
    sig=[sig,1 1 1.1 1 0.9 1 1 1.1 1 0.9 1 1.1 1 1 0.9 1 1 1.1 1 1 1 1 1.1 0.9 1 1.1 1 1 0.9 1 1.1 1 1 1.1 1 0.8 0.9 1];
    dt=0.01;
    t=[1:length(sig)]*dt;
    
    threshCntRising=2; % threshold for detecting top of the peak
    threshCntFalling=2; % remember: you can only detect a peak when both thresholds are exceeded 
    riseThresh=0.05; % (%) percent of total range
elseif signalSelect==3
    load('uas_vertical_profile1.mat')
    sig=elev_sm; % (m) GPS elevation
    t=t_gps_sm-t_gps_sm(1); % (s)
    %t=datetime(t_gps_sm, 'ConvertFrom', 'posixtime','TimeZone','local'); % 'America/New_York'
    
    threshCntRising=30; % threshold for detecting top of the peak
    threshCntFalling=5; % remember: you can only detect a peak when both thresholds are exceeded 
    riseThresh=0.03; % (%) percent of total range
elseif signalSelect==4
    load('uas_vertical_profile2.mat')
    sig=elev_sm; % (m) GPS elevation
    t=t_gps_sm-t_gps_sm(1); % (s)
    %t=datetime(t_gps_sm, 'ConvertFrom', 'posixtime','TimeZone','local'); % 'America/New_York'
    
    threshCntRising=20; % threshold for detecting top of the peak
    threshCntFalling=5; % remember: you can only detect a peak when both thresholds are exceeded 
    riseThresh=0.03; % (%) percent of total range
end

n=length(t);

% -----------------------------------------------------
% ------------------------ FIR ------------------------
% -----------------------------------------------------
windowLen=5;
sig_fir = fir(t,sig,windowLen);

% -----------------------------------------------------
% ------------------------ IIR ------------------------
% -----------------------------------------------------
tau=0.05;
sig_iir = iir(t,sig,tau);


% -----------------------------------------------------
% ------------------------ IIR ------------------------
% -----------------------------------------------------
% Robust peak detection algorithm (using z-scores)
lag = 20;
threshold = 5;
influence = 0.0;
[sig_zscores,avg,dev] = thresholdingAlgo_with_Z_scores(sig,lag,threshold,influence);









%% figure 1
if 0
    figure(1); clf %subplot(2,1,1);
    hold on;
    x = 1:length(sig); ix = lag+1:length(sig);
    area(x(ix),avg(ix)+threshold*dev(ix),'FaceColor',[0.9 0.9 0.9],'EdgeColor','none');
    area(x(ix),avg(ix)-threshold*dev(ix),'FaceColor',[1 1 1],'EdgeColor','none');
    plot(x(ix),avg(ix),'LineWidth',1,'Color','cyan','LineWidth',1.5);
    plot(x(ix),avg(ix)+threshold*dev(ix),'LineWidth',1,'Color','green','LineWidth',1.5);
    plot(x(ix),avg(ix)-threshold*dev(ix),'LineWidth',1,'Color','green','LineWidth',1.5);
    plot(x(ix),sig(ix),'b');
    %subplot(2,1,2);
    %stairs(signals,'r','LineWidth',1.5); ylim([-1.5 1.5]);
    grid on
end




%% figure 2
figure(2),clf
h1=plot(t,sig,'Color',[0.7 0.7 0.7]);
hold on
h2=plot(t,sig_iir,'b');
xlabel('time (s)')
ylabel('signal')
title_str=sprintf('signalSelect=%i',signalSelect);
title(title_str)
grid on

%h2=plot(t,sig_fir,'g.-');
%h3=plot(t,sig_iir,'b.-');
%h4=plot(t,sig_zscores,'r.-');

%firStr=strcat('FIR n=',num2str(windowLen));
%iirStr=sprintf('IIR tau=%2.5f(s)',tau);
%zscoresStr=sprintf('z-scores lag=%i, threshold=%i, influence=%f',lag,threshold,influence);

% Custom peak detection algorithm #1
[peak_t1,peak_y1] = detect_peaks(t,sig_iir,threshCntRising,threshCntFalling);
plot(peak_t1,peak_y1,'.','MarkerEdgeColor','c','MarkerSize',18); %[0.5 0.2 0.5]) % plot peaks


% Custom peak detection algorithm #2
%[peak_t2,peak_y2,risingFlagLog,rise_log] = detect_peaks_with_rise(t,sig_iir,threshCntRising,threshCntFalling,riseThresh);
[peak_t2,peak_y2,risingFlagLog,rise_log] = detect_peaks_with_rise(t,sig_iir,threshCntRising,threshCntFalling,riseThresh);
plot(peak_t2,peak_y2,'ro','MarkerSize',10) % plot peaks with rises that exceed the threshold 
for j=1:length(peak_t2)
    text(peak_t2(j),peak_y2(j)+0.5,num2str(j),'VerticalAlignment','bottom','HorizontalAlignment','center','Color','b')
end



%legend('signal',firStr,iirStr,zscoresStr,'peaks','location','best');
legend('Original signal','IIR - Infinite','peak detect','peak detect with rise','location','best');

if (makeFigureImages==1) hf=gcf; printStr=sprintf('print(%i,''outputs\\figure_%i_signalselect_%i.png'',''-dpng'')',hf.Number,hf.Number,signalSelect); eval(printStr), end








